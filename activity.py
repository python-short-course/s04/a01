from abc import ABC

class Animal(ABC):
    def __init__(self, name: str, breed: str,  age: int) -> None:
        self.name: str = name
        self.breed: str = breed
        self.age: int = age

    def eat(self, food: str) -> None:
        print(f"Eaten {food}")

    def make_sound(self) -> None:
        pass



class Cat(Animal):
    def __init__(self, name: str, breed: str, age: int) -> None:
        super().__init__(name, breed, age)
        
    ##########################################

    def get_name(self) -> str:
        return self.name
    
    def get_breed(self) -> str:
        return self.breed
    
    def get_age(self) -> int:
            return self.age
    
    ##########################################

    def set_name(self, new_name: str) -> None:
        self.name = new_name
    
    def set_breed(self, new_breed: str) -> None:
        self.breed = new_breed
    
    def set_age(self, new_age: int) -> None:
        self.age = new_age

    ##########################################

    def make_sound(self) -> None:
        print("Meow")

    ##########################################

    def call(self) -> None:
        print(f"Come to me, {self.name}")

    ##########################################

    def eat(self, food: str) -> None:
        print(f"Give me {food}")



class Dog(Animal):
    def __init__(self, name: str, breed: str, age: int) -> None:
        super().__init__(name, breed, age)

    ##########################################

    def get_name(self) -> str:
        return self.name
    
    def get_breed(self) -> str:
        return self.breed
    
    def get_age(self) -> int:
            return self.age
    
    ##########################################

    def set_name(self, new_name: str) -> None:
        self.name = new_name
    
    def set_breed(self, new_breed: str) -> None:
        self.breed = new_breed
    
    def set_age(self, new_age: int) -> None:
        self.age = new_age

    ##########################################

    def make_sound(self) -> None:
        print("Woof")

    ##########################################

    def call(self) -> None:
        print(f"Here, {self.name}")



dog: Dog = Dog("Doggy", "Aspin", 5)
dog.eat("Corned Beef")
dog.make_sound()
dog.call()

cat: Cat = Cat("Catty", "Puspin", 4)
cat.eat("fish")
cat.make_sound()
cat.call()